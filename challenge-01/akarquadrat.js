const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function akarQuadrat (x) {
    let hitung = x ** 0.5;
    
    const hasil = console.log(`Hasil akar kuadrat dari ${x} adalah ${hitung}`);
    return hasil
};

rl.question('Masukan nilai a : ', (a) => {
    let x = +(a);

    akarQuadrat(x);

    rl.close();
});
