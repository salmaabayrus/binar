const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function luasPersegi (sisi) {
    const hitung = sisi ** 2;
    
    const hasil = console.log(`Hasil dari perhitungan luas persegi adalah ${hitung}`);
    return hasil
};

rl.question('Masukan nilai sisi persegi : ', (a) => {
    let x = +(a);

    luasPersegi(x);

    rl.close();
});

