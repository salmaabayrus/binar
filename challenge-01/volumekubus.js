const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function volumeKubus (sisi) {
    const hitung = sisi ** 3;
    
    const hasil = console.log(`Hasil perhitungan volume kubus adalah ${hitung}`);
    return hasil
};

rl.question('Masukan nilai sisi : ', (a) => {
    let x = +(a);

    volumeKubus(x);

    rl.close();
});