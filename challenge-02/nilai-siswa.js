const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function getNilai(question) {
  return new Promise(resolve => {
    rl.question(question, a => {
      resolve(a);
    });
  });
};

async function olahNilai() {
  let arr = [];
  let data;

  while (data !== 'q') {
    data = await getNilai('Nilai Siswa : ');
    arr.push(data)
    if (data === 'q') {
      rl.close();
      arr.pop();
      const nilaiMax = Math.max.apply(null, arr);
      const nilaiMin = Math.min.apply(null, arr);
      let total = 0;
      arr.forEach(item => {
        total += +(item); 
      });
      const mean = total / arr.length
      let tidakLulus = 0;
      arr.forEach(item => {
        if (item < 75) {
          tidakLulus++
        };
      })
      return console.log(`Nilai Maksilmal : ${nilaiMax} \nNilai Minimum : ${nilaiMin} \nRata-rata Nilai : ${mean} \nJumlah Siswa Tidak Lulus : ${tidakLulus}`);
    };
  };
};

olahNilai();